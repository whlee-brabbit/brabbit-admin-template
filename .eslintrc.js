module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/standard',
    'eslint:recommended',
    '@vue/typescript'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'quotes': ['error', 'single'],
    'no-duplicate-imports': 'error',
    // 'indent': [ 'error', 2 ],
    'brace-style': [ 'error', '1tbs' ],
    'camelcase': [2, { 'properties': 'never' }],
    'skipBlankLines': true,
    'space-before-blocks': [
      'error',
      'always'
    ]
  },
  parserOptions: {
    ecmaVersion: 6,
    parser: '@typescript-eslint/parser'
  }
}
